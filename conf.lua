function love.conf(w)
    w.window.width          =       1280
    w.window.height         =       720
    w.window.title          =       "Powered With SuperLitium"
    w.console               =       true
    w.identity              =       "superLitium"
    w.window.resizable      =       false 
end
